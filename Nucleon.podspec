Pod::Spec.new do |s|
  s.name                  = 'Nucleon'
  s.version               = '1.10.0'
  s.summary               = 'Nucleon SDK'
  s.homepage              = 'https://beintoo.net'
  s.author                = { 'Fabrizio Bellomo' => 'fbellomo@beintoo.com' }
  s.platform              = :ios
  s.source                = { :git => 'https://github.com/Beintoo/nucleon-ios-sdk-next.git' }
  s.vendored_frameworks   = 'Nucleon.framework'
  s.module_name           = 'Nucleon'
  s.ios.deployment_target = '10.0'
  s.swift_version         = '5.1'
  s.requires_arc          = true
  s.license               = { :type => 'Copyright',
                              :text => 'Copyright (c) 2018 Beintoo, Spa. All rights reserved.' }
end
